module.exports = function(RED) {
  "use strict";
  var hids = require('./node-usb-barcode-scanner/usbscanner');
  var usbDetect = require('usb-detection');
  var usbScanner = hids.usbScanner;
  var getDevices = hids.getDevices;
  var scanners = {};
  var scannersConfig = {};

  function BarcodeScannerNode(n) {
    // Create a RED node
    RED.nodes.createNode(this, n);

    var timeOutFindScanners = null;
    var node = this;

    node.log("Init.");

    usbDetect.startMonitoring();
    node.log("Usb-detect: startMonitoring.");

    usbDetect.on('change', function(device) {
      node.log("USB device added or removed. Will rescan all devices.");
      setFindScanners(1000);
    });

    //initialize new usbScanner
    setFindScanners(1000);

    function initScanners() {
      if (Object.keys(scannersConfig).length > 0) {
        node.log("Scanners: initScanners()");
        Object.keys(scannersConfig).forEach(function(key) {
          if (!scannersConfig[key].processed) {
            scanners[key] = new usbScanner({
              devicePath: scannersConfig[key].devicePath,
              serialNumber: scannersConfig[key].serialNumber,
            });

            scannersConfig[key].processed = true;
          }

          scanners[key].removeAllListeners('data');
          scanners[key].removeAllListeners('error');

          //scanner emits a data event once a barcode has been read and parsed
          scanners[key].on("data", function(code, serialNumber) {
            node.log("Received code : " + code);

            var msg = {};
            msg.topic = node.topic;
            msg.payload = {
              code: code,
              serialNumber: serialNumber,
            };
            // send out the message to the rest of the workspace.
            // ... this message will get sent at startup so you may not see it in a debug node
            node.send(msg);
          });

          scanners[key].on("error", function(error) {
            node.error(error);
          });
        });
      }
    }

    function stopScanner(serialNumber) {
      scanners[serialNumber].stopScanning();

      delete scanners[serialNumber];
      delete scannersConfig[serialNumber];

      node.log("Scanners: StopScanning for '" + serialNumber + "'");
    }

    function stopCurrentScanners() {
      if (Object.keys(scanners).length > 0) {
        Object.keys(scanners).forEach(function(k) {
          stopScanner(k);
        });
      }
    }

    function findScanners() {
      clearTimeout(timeOutFindScanners);

      const barcodeDevices = getBarcodeDevices();

      Object.keys(scanners).forEach(function(serialNumber) {
        if (typeof barcodeDevices.find(device => device.serialNumber === serialNumber) === 'undefined') {
          stopScanner(serialNumber);
        }
      });

      if (barcodeDevices.length > 0) {
        //get array of attached HID devices
        barcodeDevices.forEach(function(device) {
          if (!scannersConfig.hasOwnProperty(device.serialNumber)) {
            scannersConfig[device.serialNumber] = {
              devicePath: device.path,
              serialNumber: device.serialNumber,
              processed: false,
            };

            node.log("Found Barcode Scanner at: " + device.path);
          }
        });

        initScanners();
      }
      else {
        node.log("No barcode scanner found.");
      }
    }

    function setFindScanners(time) {
      timeOutFindScanners = setTimeout( function () {
        findScanners();
      }, time);
    }

    function getBarcodeDevices() {
      return getDevices().filter(device => device.product && device.product.toLowerCase().indexOf('bar code') !== -1);
    }

    this.on('close', function(removed, done) {
      clearTimeout(timeOutFindScanners);

      if (removed) {
        usbDetect.stopMonitoring();
        node.log("Usb-detect: stopMonitoring.");
        stopCurrentScanners();
      }

      node.log("Restarting...");

      done();
    });
  }

  // Register the node by name. This must be called before overriding any of the
  // Node functions.
  RED.nodes.registerType("barcode-scanner", BarcodeScannerNode);
}
